package hieu19.chatroom.model;

public enum MessageType {

    CHAT,
    JOIN,
    LEAVE,
    TYPING
}
