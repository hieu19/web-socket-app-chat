'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var onTypingArea = document.querySelector('#typing-indicator');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;

var typingUsers = [];

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];
// establish connection when user click on join chat (#1)
function connect(event) {
    username = document.querySelector('#name').value.trim();

    if(username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}

// send a message when user click on join chat (#2)
function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/public', onMessageReceived);

    // Tell your username to the server
    stompClient.send("/app/chat.addUser", {}, JSON.stringify({sender: username, type: 'JOIN'})
    )

    connectingElement.classList.add('hidden');
}

// send a websocket-message to broker when user send message
function sendMessage(event) {
    var messageContent = messageInput.value.trim();
    if(messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            content: messageInput.value,
            type: 'CHAT'
        };
        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}

function handleTyping() {
    var existingTypingMessage = document.querySelector('.' + username + '-typing-message');
    if(!existingTypingMessage){
        console.log(username + "is typing...");
        console.log(username + '-typing-message');
        console.log(existingTypingMessage);
        var typingMessage = {
            sender: username,
            content: "is typing...",
            type: 'TYPING'
        };
        stompClient.send("/app/chat.onTyping", {}, JSON.stringify(typingMessage));
    }
}

// function to append html tags into the message area
function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' joined!';
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
    } else if (message.type === 'TYPING') {
        messageElement.classList.add(message.sender + '-typing-message');
        message.content = message.sender + ' is typing...!';

        typingUsers.push(message.sender);
        console.log(typingUsers);

        setTimeout(function () {
            // Loop through the list and remove the typing message elements for all users
            typingUsers.forEach(function (user) {
                var existingTypingMessage = document.querySelector('.' + user + '-typing-message');
                if (existingTypingMessage) {
                    existingTypingMessage.remove();
                }
            });

            // Clear the global list
            // typingUsers = [];
        }, 1000);
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);
    messageElement.appendChild(textElement);
    if (message.type === 'TYPING'){
        // TODO: separate typing indicator with message area
        // onTypingArea.appendChild(messageElement);
        messageArea.appendChild(messageElement);
    } else {
        messageArea.appendChild(messageElement);
    }

    messageArea.scrollTop = messageArea.scrollHeight;
}


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }
    var index = Math.abs(hash % colors.length);
    return colors[index];
}

function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}

usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', sendMessage, true)
